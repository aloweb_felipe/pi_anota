<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ],[
            'password.min' => 'A senha deve conter no minimo 6 caracteres',
            'password.confirmed' => 'A senha e sua confirmação devem ser iguais',
            'password.string' => 'A senha não deve conter caracteres especiais',
            'password.required' => 'A senha é requerida',
            'name.required' => 'O nome é requerido',
            'name.string' => 'O nome não deve contere caracteres especiais',
            'name.max' => 'O nome deve conter no máximo 255 caracteres',
            'email.required' => 'O e-mail é requerido',
            'email.string' => 'O e-mail não deve conter caracteres especiais',
            'email.email' => 'O e-mail deve ser um e-mail válido',
            'email.unique' => 'E-mail já cadastrado',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
