<!-- Left Sidebar Start -->


<div class="left side-menu" style="background-color: #dcccdc;
        border: 0;
        box-shadow: 2px 2px 3px #5f565f;
margin-top: -30px; margin-left: 20px; max-height: 93%">
    <div class="sidebar-inner slimscrollleft">
        <br>
        <div class="clearfix"></div>
        <!--- Profile -->
        <div class="profile-info">
            <div class="col-xs-4">
                <a href="profile.html" class="rounded-image profile-image">
                    <img src="/img/user/batman.png">
                </a>
            </div>
            <div class="logo hidden-sm hidden-xs">
                <h1><a href="#"><img style="width: 50% !important; height: 45px !important; margin-left: -45px !important" src="/img/logo/logo.png" alt="Logo"></a></h1>
            </div>
            <div class="row container" style="text-align: center">
            <div class="col-xs-12 col-lg-12 col-sm-12 col-md-12">
                <div class="profile-text" style="color: #121373; font-size: 26px;">Olá, <b>{{Auth::user()->name}}</b></div>
                <teste class="hidden-sm hidden-xs" style="font-size: 20px;">Level: 4</teste>
                <teste class="visible-sm visible-xs" style="font-size: 20px; margin-left: -10px; padding:0">Level: 4</teste>
                <div class="profile-buttons">
                    <div style="display: inline-block" class="row">
                        <teste style="font-size: 20px; float: left">4</teste>
                        <teste style="font-size: 20px; float: right;">5</teste>
                    <div style="
                      margin-left: 15px;
                      margin-top: 6.5px;
                      margin-right: 15px;
                      height: 15px;
                      width: 150px;
                      border-radius: 15px;
                      background:#F65314;
                      background: -moz-linear-gradient(left, rgba(6,13,144,1) 30.20%, #f2f2f2 46.80%);
                      background: -webkit-linear-gradient(left, rgba(6,13,144,1) 30.20%, #f2f2f2 46.80%);
                      background: linear-gradient(left, rgba(6,13,144,1) 30.20%, #f2f2f2 46.80%);">
                    </div>
                    </div>
                    <br>
                    <span style="padding-left: 25px">37% concluido</span>
                </div>
            </div>
            </div>
        </div>
        <!--- Divider -->
        <div class="clearfix"></div>
        <hr class="divider"/>
        <div class="clearfix"></div>
        <!--- Divider -->
        <div style="width: 90%; margin-left: 15px">
        {{--<select name="" id="" class="form-control hidden-sm hidden-xs" style="border-radius: 10px">--}}
            {{--<option value="inicio">Inicio</option>--}}
            {{--<option value="editar_perfil">Editar perfil</option>--}}
            {{--<option value="avaliacao">Avaliação</option>--}}
            {{--<option value="provas">Provas</option>--}}
            {{--<option value="suporte">Suporte</option>--}}
        {{--</select>--}}

            <div id="sidebar-menu">
                <ul>
                    <li><a href='{{route('home')}}' id="home" class='active'><i class="icon-home-3"></i><span>Inicio</span></a></li>
                    <li><a href='{{route('perfil')}}' id="perfil"><i class="icon-person"></i><span>Editar perfil</span></a></li>
                    <li><a href='{{route('avaliacao')}}' id="avaliacao"><i class="icon-star"></i><span>Avaliação</span></a></li>
                    <li><a href='{{route('provas')}}' id="provas"><i class="fa fa-space-shuttle"></i><span>Provas</span></a></li>
                    <li><a href='{{route('suporte')}}' id="suporte"><i class="icon-reddit"></i><span>Suporte</span></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <script>

                var urlAtual = window.location.href;
                var home = document.getElementById('home');
                var perfil = document.getElementById('perfil');
                var avaliacao = document.getElementById('avaliacao');
                var provas = document.getElementById('provas');
                var suporte = document.getElementById('suporte');
                console.log(urlAtual);

                if(urlAtual == 'http://localhost:8000/'){
                    home.classList.add('active');
                    avaliacao.classList.remove('active');
                    provas.classList.remove('active');
                    suporte.classList.remove('active');
                    perfil.classList.remove('active');
                }
                    if(urlAtual == 'http://localhost:8000/perfil'){
                    home.classList.remove('active');
                    avaliacao.classList.remove('active');
                    provas.classList.remove('active');
                    suporte.classList.remove('active');
                    perfil.classList.add('active');
                }
                if(urlAtual == 'http://localhost:8000/avaliacao'){
                    home.classList.remove('active');
                    avaliacao.classList.add('active');
                    provas.classList.remove('active');
                    suporte.classList.remove('active');
                    perfil.classList.remove('active');
                }
                if(urlAtual == 'http://localhost:8000/provas'){
                    home.classList.remove('active');
                    avaliacao.classList.remove('active');
                    provas.classList.add('active');
                    suporte.classList.remove('active');
                    perfil.classList.remove('active');
                }
                if(urlAtual == 'http://localhost:8000/suporte'){
                    home.classList.remove('active');
                    avaliacao.classList.remove('active');
                    provas.classList.remove('active');
                    suporte.classList.add('active');
                    perfil.classList.remove('active');
                }


            </script>

        {{--<select name="" id="" class="form-control visible-sm visible-xs" style="border-radius: 10px; margin-left: -13px">--}}
            {{--<option value="inicio">Inicio</option>--}}
            {{--<option value="editar_perfil">Editar perfil</option>--}}
            {{--<option value="avaliacao">Avaliação</option>--}}
            {{--<option value="provas">Provas</option>--}}
            {{--<option value="suporte">Suporte</option>--}}
        {{--</select>--}}


        </div>
        <div class="clearfix"></div>
        <br><br><br><br><br><br><br><br><br><br>
        <center>
        <button class="btn btn-primary md-trigger hidden-sm hidden-xs" style="background-color: #393d73" data-modal="logout-modal">
            <i class="icon-logout-1"></i> Sair
        </button>

        <button class="btn btn-primary md-trigger visible-sm visible-xs" style="background-color: #393d73" data-modal="logout-modal">
            <i class="icon-logout-1"></i>
        </button>
        </center>
    </div>
</div>
<!-- Left Sidebar End -->

