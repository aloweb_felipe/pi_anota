<script>
    var resizefunc = [];
</script>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/js/template/jquery-1.11.1.min.js"></script>
<script src="/js/template/bootstrap.min.js"></script>
<script src="/js/template/fastclick.js"></script>
<script src="/js/template/jquery-sparkline.js"></script>
<script src="/js/template/jquery-slimscroll/jquery.slimscroll.js"></script>
<!--Modal-->
<script src="/js/template/nifty-modal/js/classie.js"></script>
<script src="/js/template/nifty-modal/js/modalEffects.js"></script>
<script src="/js/template/jquery-detectmobile/detect.js"></script>
<script src="/js/template/prettify/prettify.js"></script>
<!--Fim modal-->
<script src="/js/template/init.js"></script>
