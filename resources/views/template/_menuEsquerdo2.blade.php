<!-- Left Sidebar Start -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <br>
        <div class="clearfix"></div>
        <!--- Profile -->
        <div class="profile-info">
            <div class="col-xs-4">
                <a href="profile.html" class="rounded-image profile-image">
                    <img src="/img/user/batman.png">
                </a>
            </div>
            <div class="col-xs-8">
                <div class="profile-text">Olá <b>{{Auth::user()->name}}</b></div>
                <strong style="color: white">Level: 4</strong>
                <div class="profile-buttons">
                    <div style="display: inline-block" class="row">
                        <strong style="color: white; float: left">4</strong>
                        <strong style="color: white; float: right;">5</strong>
                    <div style="
                      margin-left: 15px;
                      margin-top: 2px;
                      margin-right: 15px;
                      height: 15px;
                      width: 150px;
                      border-radius: 15px;
                      background:#F65314;
                      background: -moz-linear-gradient(left, rgba(6,13,144,1) 30.20%, #f2f2f2 46.80%);
                      background: -webkit-linear-gradient(left, rgba(6,13,144,1) 30.20%, #f2f2f2 46.80%);
                      background: linear-gradient(left, rgba(6,13,144,1) 30.20%, #f2f2f2 46.80%);">
                    </div>
                    </div>
                    <span style="color: white; padding-left: 25px"><strong>37% concluido</strong></span>
                </div>
            </div>
        </div>
        <!--- Divider -->
        <div class="clearfix"></div>
        <hr class="divider"/>
        <div class="clearfix"></div>
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li><a href='{{route('home')}}' class='active'><i class="icon-home-3"></i><span>Dashboard</span></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
        <div class="portlets">
            <div id="recent_tickets" class="widget transparent nomargin">
                <h2>by Felipe</h2>
            </div>
        </div>
        <div class="clearfix"></div>
        <br><br><br>
    </div>
</div>
<!-- Left Sidebar End -->

