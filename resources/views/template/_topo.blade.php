<!-- Top Bar Start -->
<div class="topbar">
    <div class="topbar-left">
        <div class="logo">
            <h1><a href="#"><img style="width: 50% !important; height: 45px !important; margin-left: -45px !important" src="/img/logo/logo.png" alt="Logo"></a></h1>
        </div>
        <button class="button-menu-mobile open-left">
            <i class="fa fa-bars"></i>
        </button>
    </div>
    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="navbar-collapse2">

                <ul class="nav navbar-nav navbar-right top-navbar" style="padding-right: 15px">


                    <li class="dropdown iconify hide-phone"><a href="#" onclick="javascript:toggle_fullscreen()"><i
                                    class="icon-resize-full-2"></i></a>
                    </li>
                    <li class="dropdown topbar-profile" >
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
                                    class="rounded-image topbar-profile-image"><img
                                        src="/img/user/batman.png"></span> <strong>{{Auth::user()->name}}</strong>
                            <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu" >
                            <li><a href="#">Perfil</a></li>
                            <li><a href="#">Trocar senha</a></li>
                            <li class="divider"></li>
                            <li><a href="#"><i class="icon-help-2"></i> Ajuda</a></li>
                            <li><a href="#"><i class="icon-lock-1"></i> Trancar</a></li>
                            <li><a class="md-trigger" data-modal="logout-modal">
                                    <i class="icon-logout-1"></i> Logout</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
    </div>
</div>
<!-- Top Bar End -->