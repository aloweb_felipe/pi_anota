<style>
    .tela_grande{
        max-width: 72% !important;
    }
    .tela_pequena{
        max-width: 90.2% !important;
    }
    .box-text-anotacao{
        background-color: #dcccdc;
        border: 0;
        box-shadow: 2px 2px 3px #5f565f;
        height: 120px;
        /*margin-left: -15px !important;*/
        max-width: 900px;
        /*width: 250px !important;*/
    }
    .box-cachorro{
        border: 0;
        background-color: #dcccdc;
        box-shadow: 2px 2px 3px #5f565f;
        height: 120px;
        text-align: center;
        /*float: right;*/
        /*margin-right: 47px;*/
        /*max-width: 120px*/

    }
    .box-todo-anotacao-cachorro{
        max-width: 72%;
        text-align: center;
        padding-bottom: 25px;
        position: fixed;
        bottom: 0;
    }
    @media screen and (max-width: 400px){
       .box-cachorro{
           max-width: 100px !important;
       }
    }
</style>

    <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12 box-todo-anotacao-cachorro" id="anotacao_cao">
        <div class="col-lg-9 col-sm-8 col-md-9 col-xs-6 box-text-anotacao" style="padding-top: 0px;">
            <br>
            <h3 style="color: #3366ff; margin-top: 25px !important;"><strong>Olá, {{auth()->user()->name}}</strong></h3>
        </div>
        <div class="col-lg-1 col-md-1 col-sm-1"></div>
        <div class="col-lg-2 col-sm-3 col-md-2 col-xs-6 box-cachorro">
            <img src="/img/anotacao/anotacao.png" style="padding-top: 8px; max-width: 100%; max-height: 100%" alt="" >
        </div>
    </div>
<br><br><br><br>

