@extends('template.master')

@section('conteudo')
    <style>
        .imagem_grande{
            padding-bottom: 20px;
        }
    </style>

    <div style="background-color: #dcccdc;
        border: 0;
        box-shadow: 2px 2px 3px #5f565f;
text-align: center;
vertical-align: middle; height: 75px; margin-bottom: 15px;">
        <h1 style="color: #3366ff; padding-top: 15px"><strong>Curso de lógica de programação</strong></h1>
    </div>
    <div style="background-color: #dcccdc; border: 0; box-shadow: 2px 2px 3px #5f565f;">
        <div class="container" style="padding-top: 10px; padding-bottom: 10px">
            <div class='container' style="background-color: #93b6cb; border: 2px solid #4c60cb; padding-right: 25px; padding-bottom: 25px; padding-top: 25px; margin:25px">
                <div class="row">
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aula1.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-aberto.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aula2.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-aberto.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <a href="{{route('avaliacao')}}"><img src="/img/telaInicial/aula3.png" class="imagem_grande" alt="" style=""></a>
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/aulas-fechadas.png" class="imagem_grande" alt="" style="">
                    </div>
                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-3">
                        <img src="/img/telaInicial/presente-fechado.png" class="imagem_grande" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection