<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('perfil', 'PerfilController@index')->name('perfil');
Route::get('avaliacao', 'AvaliacaoController@index')->name('avaliacao');
Route::get('provas', 'ProvasController@index')->name('provas');
Route::get('suporte', 'SuporteController@index')->name('suporte');

